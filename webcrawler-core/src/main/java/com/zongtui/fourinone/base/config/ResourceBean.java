package com.zongtui.fourinone.base.config;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * 资源对象.
 */
public class ResourceBean {
    //资源名称.
    protected String resourcesName;
    //国际化资源对象.
    protected ResourceBundle bundle;

    public ResourceBean() {
    }

    public ResourceBean(String resourcesName) {
        bundle = ResourceBundle.getBundle(resourcesName);
    }

    /**
     * 获取资源.
     *
     * @param keyWord 属性名称.
     * @return 资源属性值
     */
    public String getString(String keyWord) {
        String str = "";
        try {
            str = bundle.getString(keyWord);
        } catch (MissingResourceException ex) {
            //noinspection ThrowablePrintedToSystemOut
            System.err.println(ex);
        }
        return str;
    }

    public static void main(String[] args) {
        ResourceBean rb = new ResourceBean("config");
        System.out.println(rb.getString("QSXYSJ"));

        System.out.println(Thread.currentThread().getContextClassLoader().getResource(""));
        System.out.println(ResourceBean.class.getClassLoader().getResource(""));
        System.out.println(ClassLoader.getSystemResource(""));
        System.out.println(ResourceBean.class.getResource(""));
        System.out.println(ResourceBean.class.getResource("/"));
        System.out.println(new java.io.File("").getAbsolutePath());
        System.out.println(System.getProperty("user.dir"));
    }
}
