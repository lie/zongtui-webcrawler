/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.zongtui.web.test.dao;

import com.zongtui.web.common.persistence.CrudDao;
import com.zongtui.web.common.persistence.annotation.MyBatisDao;
import com.zongtui.web.test.entity.TestData;

/**
 * 单表生成DAO接口
 * @author ThinkGem
 * @version 2015-04-06
 */
@MyBatisDao
public interface TestDataDao extends CrudDao<TestData> {
	
}